/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led1 = 5;
int led2 = 6;
int led3 = 9;
int POT = A0;

// the setup routine runs once when you press reset:
void setup() {                
  Serial.begin(9600);
  
  // initialize the digital pin as an output.
  pinMode(led1, OUTPUT);     
  pinMode(led2, OUTPUT);   
  pinMode(led3, OUTPUT);    
}

// the loop routine runs over and over again forever:
void loop() {
  
  int reading = analogRead(POT);
  int l1 = reading / 4;
  int l2 = (767 - abs(reading - 511))/4;
  int l3 = (1023-reading) / 4;
  
  analogWrite(led1, l1);
  analogWrite(led2, l2);
  analogWrite(led3, l3);
  
  Serial.println(reading);
  
  delay(100);
  
}
