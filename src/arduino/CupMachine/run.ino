void setup() {
  // initialize serial:
  Serial.begin(9600);
  Serial1.begin(9600);
  
  // setup for the motor
  pinMode(DIRECTION, OUTPUT); //Initiates Motor Channel B pin
  pinMode(BRAKE, OUTPUT); //Initiates Brake Channel B pin
  
  digitalWrite(DIRECTION, LOW); //Establishes forward direction of Channel A
  digitalWrite(BRAKE, LOW);   //Disengage the Brake for Channel A

}

void loop(){
  /* // Check and set the state of the hall effect sensor
  hallState = digitalRead(HALL_SENSOR);
  
  // Run the motor if the magnet is in front of it
  if( hallState == LOW){//HIGH ){
    sendMessage("Signal Received");
    delay(MOTOR_DELAY); // Wait before running the motor
    sendMessage("Starting Motor");
    runMotor();
    delay(MOTOR_RUNTIME); // Continue running the motor for x time.
    sendMessage("Motor Finished");
  } else if (flexTriggered()) { //TODO: Add a delay to this happening.
    delay(200);
    stopMotor();
  }*/
  motorStateMachine.update();
  proximityStateMachine.update();
  //delay(1);
  //Serial.println(analogRead(P_PROXIMITY_SENSOR));
  delay(100);
}
