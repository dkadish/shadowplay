/* 
  Hall Processing
  
  Uses a Hall Effect Sensor and the Arduino WiFi Module to send a signal to processing.
  
  Created 2013
  by David Kadish
  
  */
#include <FiniteStateMachine.h>

// Motor Finite State Machine Elements
State resting = State(checkForCup);
State cup_received = State(initCupReceived, updCupReceived, runMotor);
State motor = State(checkForThreshold);
State threshold = State(initThreshold, updThreshold, stopMotor);

FSM motorStateMachine = FSM(resting);

// Proximity Finite State Machine
State searching = State(handleProximitySensor);
State waiting = State(handleProximityWaiting);
State wait_delay = State(initProximityDelay, handleProximityDelay, NULL);

FSM proximityStateMachine = FSM(searching);

long cupReceivedMillis = 0;
long thresholdMillis = 0;

// constants won't change. They're used here to set pin numbers:
// hall effect sensor
const int HALL_SENSOR = 7;

// motor
const int DIRECTION = 12;
const int SPD = 3;
const int BRAKE = 9;
const int CURRENT = 0;

// motor timing
const int MOTOR_DELAY = 500;
const int MOTOR_RUNTIME = 2000;
const int THRESHOLD_DELAY = 500;

// flex sensor
const int P_FLEX = A2;
const int FLEX_THRESH = 90;

// variables will change:
int hallState = LOW;         // variable for reading the pushbutton status
int proximity_millis = 0;

void sendMessage(char s){
  Serial.print(s);
  Serial1.print(s);
}
