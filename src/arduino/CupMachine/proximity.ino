// Proximity Sensor
const int P_PROXIMITY_SENSOR = A3;
const int PROXIMITY_THRESH = 150;
const int PROXIMITY_DELAY = 3000;

void handleProximitySensor(){
  int prox = analogRead(P_PROXIMITY_SENSOR);
  Serial.println("Searching: " + prox);
  if( prox < PROXIMITY_THRESH ){
    Serial.print('a');
    Serial1.print('a');
  }
}

void handleProximityWaiting(){
  Serial.println("Proximity Waiting");
}

void initProximityDelay(){ proximity_millis = millis(); }

void handleProximityDelay(){
  Serial.println("Delaying");
  if( millis() - proximity_millis >= PROXIMITY_DELAY ){
    proximityStateMachine.transitionTo(searching);
  }
}

