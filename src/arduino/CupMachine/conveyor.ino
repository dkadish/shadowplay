
/*
  Runs when the box is in waiting mode. Checks for the presence
  of a cup, and then triggers the next state when it finds one.
  */
void checkForCup(){
  //Serial1.println("Checking for cup...");
  if( digitalRead(HALL_SENSOR) == HIGH ){
    sendMessage('b');
    motorStateMachine.transitionTo(cup_received);
  }
}

void initCupReceived(){ cupReceivedMillis = millis(); }

void updCupReceived(){
  //Serial1.println("Waiting for cup delay...");
  if( millis() - cupReceivedMillis >= MOTOR_DELAY ){
    motorStateMachine.transitionTo(motor);
    //proximityStateMachine.transitionTo(waiting);
  }
}

void initThreshold(){ thresholdMillis = millis(); }

void updThreshold(){
  //Serial1.println("Waiting for threshold delay...");
  if( millis() - thresholdMillis >= THRESHOLD_DELAY ){
    motorStateMachine.transitionTo(resting);
    //proximityStateMachine.transitionTo(wait_delay);
  }
}

void checkForThreshold(){
  //Serial1.println("Running motor...");
  if( flexTriggered() ){
    sendMessage('d');
    motorStateMachine.transitionTo(threshold);
  }
}

void runMotor(){
  digitalWrite(BRAKE, LOW);
  analogWrite(SPD, 255);
}

void stopMotor(){
  digitalWrite(BRAKE, HIGH);
  analogWrite(SPD, 0);
}

bool flexTriggered(){
  //Serial1.print("FLEX: ");
  int flex = analogRead(P_FLEX);
  return flex < FLEX_THRESH;
}
