/* 
  Hall Processing
  
  Uses a Hall Effect Sensor and the Arduino WiFi Module to send a signal to processing.
  
  Created 2013
  by David Kadish
  
  */
#include <WiFiClient.h>
#include <WiFi.h>
 
// constants won't change. They're used here to 
// set pin numbers:
const int buttonPin = 9;     // the number of the pushbutton pin
//const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

char ssid[] = "dd-wrt";     // the name of your network
int status = WL_IDLE_STATUS;     // the Wifi radio's status
IPAddress server(192,168,1,132);  // Processing Server

// Initialize the client library
WiFiClient client;

boolean sentMessage;
boolean isHigh;

void setup() {
/*  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      */
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  
  // initialize serial:
  Serial.begin(9600);

  // attempt to connect to an open network:
  Serial.println("Attempting to connect to open network...");
  status = WiFi.begin(ssid);

  // if you're not connected, stop here:
  if ( status != WL_CONNECTED) { 
    Serial.println("Couldn't get a wifi connection");
    while(true);
  } 
  // if you are connected :
  else {
      Serial.print("Connected to the network");
      Serial.println("\nStarting connection...");
      // if you get a connection, report back via serial:
      if (client.connect(server, 10002)) {
        Serial.println("connected");
        // Make a HTTP request:
        //client.println("Hello Processing!");
        //client.println();
      }
  }
  
  sentMessage = false;
  isHigh = false;
}

void loop(){
  // Check if we are waiting for a response
  if( !sentMessage ){
    // read the state of the pushbutton value:
    buttonState = digitalRead(buttonPin);
  
    // check if the pushbutton is pressed.
    // if it is, the buttonState is HIGH:
    if (buttonState == HIGH && !isHigh) {     
      // turn LED on:    
      client.println("high");
      isHigh = true;
      sentMessage = true;
    } 
    else if (buttonState == LOW && isHigh) {
      // turn LED off:
      client.println("low");
      isHigh = false;
      sentMessage = true;
    }
  } else {
    if ( client.available() ){
      char c = client.read();
      Serial.print(c);
      sentMessage = false;
    }
  }
}
