/** Bubble Machine using the XBee and Relay Shield
*  
*   This sketch controls a motor/fan for a bubble machine using the
*   Microbot.it Relay Shield and XBee module for communication.
*
*   Created: March 17, 2013
*   Author: David Kadish
*/
#include <FiniteStateMachine.h>

// Pin Definitions
const int P_SERVO_CTL = 9; // Servo motor control on digital pin 9
const int P_FAN_CTL = 5; // Fan controlled by relay 1 on digital pin 4
const int THRESHOLD_DELAY = 10000;

State waiting = State(checkForSignal);
State bubbling = State(initBubbling, updBubbling, stopBubbling);
FSM bubbleStateMachine = FSM(waiting);

long bubble_millis = 0;

/*String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
*/

void setup(){
  pinMode(P_SERVO_CTL, OUTPUT);
  pinMode(P_FAN_CTL, OUTPUT);
  
  // Set up serial communications
  //Serial.begin(9600); // Serial1 is the h/w serial for the Leonardo boards
  Serial1.begin(9600); // Serial1 is the h/w serial for the Leonardo boards
  // reserve 200 bytes for the inputString:
  //inputString.reserve(200);
}

void loop(){
  // print the string when a newline arrives:
  bubbleStateMachine.update();
}

void checkForSignal(){
  if( Serial1.available() ){
    char inChar = (char) Serial1.read();
    //Serial.println(inChar);
    if( inChar == 'e' ){
      //Serial.println("It's an e!");
      bubbleStateMachine.immediateTransitionTo(bubbling);
    } else {
      //Serial.println("It's NOT an e!");
    }
  }
}
   
void initBubbling(){
  bubble_millis = millis();
  
  //Start the machine
  digitalWrite(P_FAN_CTL, HIGH);
  analogWrite(P_SERVO_CTL, 5);
}

void updBubbling(){
  if( millis() - bubble_millis >= THRESHOLD_DELAY ){
    bubbleStateMachine.transitionTo(waiting);
  }
}

void stopBubbling(){
  bubble_millis = 0;
  
  //Start the machine
  digitalWrite(P_FAN_CTL, LOW);
  analogWrite(P_SERVO_CTL, 0);
  
  Serial1.print('f');
}

