/* 
  Hall Processing
  
  Uses a Hall Effect Sensor and the Arduino WiFi Module to send a signal to processing.
  
  Created 2013
  by David Kadish
  
  */

// constants won't change. They're used here to set pin numbers:
// hall effect sensor
const int HALL_SENSOR = 7;

// motor
const int DIRECTION = 12;
const int SPD = 3;
const int BRAKE = 9;
const int CURRENT = 0;

// motor timing
const int MOTOR_DELAY = 500;
const int MOTOR_RUNTIME = 2000;

// flex sensor
const int P_FLEX = A2;
const int FLEX_THRESH = 90;

// variables will change:
int hallState = LOW;         // variable for reading the pushbutton status

void setup() {
  // initialize serial:
  Serial1.begin(115200);
  
  // setup for the motor
  pinMode(DIRECTION, OUTPUT); //Initiates Motor Channel B pin
  pinMode(BRAKE, OUTPUT); //Initiates Brake Channel B pin
  
  digitalWrite(DIRECTION, HIGH); //Establishes forward direction of Channel A
  digitalWrite(BRAKE, LOW);   //Disengage the Brake for Channel A

}

void loop(){
  // Check and set the state of the hall effect sensor
  hallState = digitalRead(HALL_SENSOR);
  
  // Run the motor if the magnet is in front of it
  if( hallState == LOW){//HIGH ){
    sendMessage("Signal Received");
    delay(MOTOR_DELAY); // Wait before running the motor
    sendMessage("Starting Motor");
    runMotor();
    delay(MOTOR_RUNTIME); // Continue running the motor for x time.
    sendMessage("Motor Finished");
  } else if (flexTriggered()) { //TODO: Add a delay to this happening.
    delay(200);
    stopMotor();
  }
}

void runMotor(){
  digitalWrite(BRAKE, LOW);
  analogWrite(SPD, 255);
}

void stopMotor(){
  digitalWrite(BRAKE, HIGH);
  analogWrite(SPD, 0);
}

void sendMessage(String s){
  Serial1.println(s + '\f');
}

boolean flexTriggered(){
  Serial1.print("FLEX: ");
  int flex = analogRead(P_FLEX);
  return flex < FLEX_THRESH;
}
