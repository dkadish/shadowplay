// Pins
const int P_PROXIMITY = A3;

// Thresholds
const int PROXIMITY_THRESH = 60;

// Delays
const int PROXIMITY_DELAY = 3000;

//Variables
long proximity_millis = 0;
