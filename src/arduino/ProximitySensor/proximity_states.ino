State noPerson = State(upNoPerson); // No person has been sensed
State waitPerson = State(startWaitPerson, upWaitPerson, stopWaitPerson); // A person has been sensed, but we are in the waiting period
State Person = State(upPerson); // A person has been sensed
State waitNoPerson = State(startWaitNoPerson, upWaitNoPerson, stopWaitNoPerson); // No person has been sensed, but we are in the waiting period
//State conveyor = State(NULL); // The conveyor is running

FSM proximityMachine = FSM(noPerson);

// No person is there
void upNoPerson(){
  int prox = analogRead(P_PROXIMITY);  
  if( prox < PROXIMITY_THRESH ){
    SendMessage('a');
    proximityMachine.transitionTo(waitPerson);
  }
}

// Sensed a person
void startWaitPerson(){ proximity_millis = millis(); }

void upWaitPerson(){
  if( (millis() - proximity_millis) > PROXIMITY_DELAY ){
    proximityMachine.transitionTo(Person);
  }
}

void stopWaitPerson(){ proximity_millis = 0; }

// Person is there
void upPerson(){
  int prox = analogRead(P_PROXIMITY);  
  if( prox > PROXIMITY_THRESH ){
    SendMessage('g');
    proximityMachine.transitionTo(waitNoPerson);
  }
}

// Sensed nobody
void startWaitNoPerson(){ proximity_millis = millis(); }

void upWaitNoPerson(){
  if( millis() - proximity_millis > PROXIMITY_DELAY ){
    proximityMachine.transitionTo(noPerson);
  }
}

void stopWaitNoPerson(){ proximity_millis = 0; }
