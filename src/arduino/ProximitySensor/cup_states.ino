
// Motor Finite State Machine Elements
State resting = State(checkForCup);
State cup_received = State(startCupReceived, upCupReceived, stopCupReceived);

FSM cupMachine = FSM(resting);

// Waiting for a cup
void checkForCup(){
  //Serial1.println("Checking for cup...");
  if( digitalRead(HALL_SENSOR) == HIGH ){
    SendMessage('b');
    cupMachine.transitionTo(cup_received);
  }
}

// Got a cup
void startCupReceived(){ cupReceivedMillis = millis(); }

void upCupReceived(){
  if( millis() - cupReceivedMillis >= CUP_DELAY ){
    cupMachine.transitionTo(resting);
  }
}

void stopCupReceived(){
  cupReceivedMillis = 0;
  SendMessage('d');
}
