import processing.net.*; 
Client myClient;
int dataIn; 
int xVal;
int isOn;
String myString;
void setup() {
  size(200, 200);
  // Connect to the local machine at port 10002.
  myClient = new Client(this, "127.0.0.1", 10002);
  isOn = 0;
  xVal = -24;

}

void draw() {
  noStroke();
  fill(150, 50, 150, 10);
  rect(0, 0, width, height);
  myString = myClient.readString();
  if (myString == null) {
    myString = "";
  }
  
  if (myString.equals("201")) {
    isOn=1;
  }
  
  if (isOn == 1) {
    drawTarget(xVal);
    xVal++;
  }
  
  if (xVal > width+24) {
    //start animation2
    xVal = -24;
    isOn = 0;
  }
}

void drawTarget(int xVal) {
  for (int i = 0; i < xVal; i++) {
    fill(255);
    ellipse(xVal, 87, 25, 25);
  }
}



