
import processing.net.*;

int port = 10002;   
Server myServer;    
int val = 0;
int client1On = 0;
int client2On = 0;
int client3On = 0;
String clientVal = "";
void setup()
{
  //size(1, 1);
  //background(0);
  myServer = new Server(this, port); // Starts a server on port 10002
}

void draw() {
  
  val = (val + 1) % 255;
  background(val); 
  
  Client thisClient = myServer.available();
  // If the client is not null set trigger for new animation
  if (thisClient !=null) {

    clientVal = thisClient.readString();
    //println(clientVal.length());
    if (clientVal.equals("201")) {
      myServer.write("201");
    } else {
      println("false");
    }
  } 
  
}
