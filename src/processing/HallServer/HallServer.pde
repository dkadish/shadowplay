/**
 * Arduino Hall Effect Sensor Talking to the Server 
 * by David Kadish. 
 * 
 * Take a message from the arduino WiFi module.
 */
 

import processing.net.*;

int port = 10002;
boolean myServerRunning = true;
int bgColor = 0;
int direction = 1;
int textLine = 60;

String lineEnding = "\r\n";

Server myServer;

String msg;

void setup()
{
  size(400, 400);
  textFont(createFont("SanSerif", 16));
  myServer = new Server(this, port); // Starts a myServer on port 10002
  
  colorMode(HSB, width, 100, width);
  noStroke();
  background(0);
}

void mousePressed()
{
  // If the mouse clicked the myServer stops
  myServer.stop();
  myServerRunning = false;
}

void draw()
{
  if (myServerRunning == true)
  {
    text("server", 15, 45);
    Client thisClient = myServer.available();
    if (thisClient != null) {
      if (thisClient.available() > 0) {
        msg = thisClient.readString();
        if( msg.equals("high\r\n") ){
          println("HIGH!");
          fill(128, 155, 255);
          rect(0,0,width,height);
        } else if ( msg.equals("low\r\n") ) {
          println("LOW!");
          fill(255,155,255);
          rect(0,0,width,height);
        } else {
          print("MESSAGE: ");
          print(msg);
          print(", ");
          print(msg.equals("high\r\n"));
          print(", ");
          print(msg.equals("low\r\n"));
        }
        
        thisClient.write("thx");
        //text("mesage from: " + thisClient.ip() + " : " + thisClient.readString(), 15, textLine);
      }
    }
  } 
  else 
  {
    text("server", 15, 45);
    text("stopped", 15, 65);
  }
}
