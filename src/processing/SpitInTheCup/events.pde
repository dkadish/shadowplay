void handleEvents(char e){
  switch(e){
    case 'a':
      // We sensed a person. Start playing the animation.
      //println("Found an 'a'");
      if( cupMachine.isInState(waiting) ){
        //println("Going to Playing");
        cupMachine.transitionTo(playing);
        //println("Gone to Playing");
      } else { println(cupMachine.getCurrentState()); }
      break;
    case 'b':
      // We sensed a cup and the conveyor is starting. Stop the animation.
      println("We've got a B!");
      cupMachine.transitionTo(delaying);
      break;
    case 'g':
      // The person left the sensor area. Stop the animation.
      if( cupMachine.isInState(playing) ){
        cupMachine.transitionTo(waiting);
      }
      break;
    default:
      break;
  }
}

/* Handles the most prominent event after the video has stopped playing
*
*
*/
void delayHandleEvent(char e){
  switch(e){
    case 'a':
      // We sensed a person. Start playing the animation.
    case 'b':
      // We sensed a cup and the conveyor is starting. Stop the animation.
    case 'g':
      // The person left the sensor area. Stop the animation.
    default:
  }
}
