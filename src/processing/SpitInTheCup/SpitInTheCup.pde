/**
 * SpitInTheCup! 
 * 
 * Plays the an animation asking people to spit into a cup.
 * 
 * Plays when it senses a person. If it just played, wait a gap to play again.
 * When a person is seen, play a video.
 * At the end of that video, if a signal has been received, process it.
 */

import processing.video.*;

// Constants
final String dir = "";
final String movie_pattern = "Sequence %d.mov";
final int n_movies = 7;
final int DELAY_THRESH = 10 * 1000; // in milliseconds

int i_playing = -1;
float currentMovieTime = 0.0;

int delay_millis = 0;

ArrayList movies;

State waiting = new State(this, "startWait", "doWait", "endWait");
State playing = new State(this, "startPlay", "doPlay", "endPlay");
State delaying = new State(this, "startDelay", "doDelay", "endDelay");

FSM cupMachine;

Movie movie;

void setup() {
  size(1024, 768);
  background(0);
  
  setupNetworkClient();
  cupMachine = new FSM(waiting);

  movies = new ArrayList();

  for ( int i = 1; i <= n_movies; i++ ) {
    // Load and play the video in a loop
    Movie m = new Movie(this, String.format(movie_pattern, i));
    m.noLoop();
    m.stop();
    movies.add(m);
  }
}

void movieEvent(Movie m) {
  m.read();
}

void draw() {
  drawNetworkClient();
  cupMachine.update();
}  

void startPlay(){}

void doPlay(){
  //println("Playing!");
  if ( i_playing == -1 ) {
    i_playing = ((int)random(n_movies-1))+1;
    ((Movie)movies.get(i_playing)).play(); 
  }

  if ( ((Movie)movies.get(i_playing)).duration() - ((Movie)movies.get(i_playing)).time() < 0.1 ) {
    i_playing = ((int)random(n_movies-1))+1;
    ((Movie)movies.get(i_playing)).jump(0);
    ((Movie)movies.get(i_playing)).play();
  }

  image(((Movie)movies.get(i_playing)), 0, 0, width, height);
}

void endPlay(){
  //println("Done playing...");
  i_playing = -1;
}


void startWait(){}//println("Starting to wait");}
void doWait(){}//println("Just waiting");}
void endWait(){}//println("Finished waiting");}

void noFunc(){}//println("Nothing Doing");}

void startDelay(){
  rect(0,0,width,height);
  //println("Starting delay...");
  delay_millis = millis();
}

void doDelay(){
  if( millis() - delay_millis > DELAY_THRESH ){
    cupMachine.transitionTo(waiting);
  }
}

void endDelay(){
  delay_millis = 0;
}

