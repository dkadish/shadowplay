import processing.net.*;

Client myClient;
int dataIn; 

void setupNetworkClient() {
  // Connect to the local machine at port 5204.
  // This example will not run if you haven't
  // previously started a server on this port.
  myClient = new Client(this, "192.168.1.118", 5204);
}

void drawNetworkClient() {
  if (myClient.available() > 0) {
    char dataIn = (char) myClient.read();
    println("dataIn: " + dataIn);
    //TODO: code calling the switch statement for what to do on an event.
    handleEvents(dataIn);
  }
}

